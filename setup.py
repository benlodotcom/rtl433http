from setuptools import setup

setup(name='rtl433http',
      version='0.1',
      packages=["rtl433http"],
      install_requires=[
              'pyaml',
              'requests'
          ],
      entry_points={
          'console_scripts': [
              'rtl433http=rtl433http.rtl433http:main',
          ],
      },
      zip_safe=False)
