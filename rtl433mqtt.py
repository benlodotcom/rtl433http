#!/usr/bin/env python3

import subprocess
import json
import requests

HA_API_URL = "http://localhost:8123/api/"
HA_API_KEY = "mel010806"
SENSOR_WHITELIST_IDS = [5996, 5379]
DATA_TO_UNIT_AND_ID = {"temperature_C": {"unit": "°C", "suffix": "t"},
                       "humidity": {"unit": "%", "suffix": "h"}}


def update_sensor_ha(entity, state, measurement_unit):
    url = "%s%s/%s" % (HA_API_URL, "states", entity)
    headers = {"Content-Type": "application/json",
               "x-ha-access": HA_API_KEY}
    data = json.dumps({"state": state,
                       "attributes":
                       {"unit_of_measurement": measurement_unit}})
    print("Posting to url %s, with data %s" % (url, data))
    try:
        response = requests.post(url, headers=headers, data=data)
        response.raise_for_status()
    except:
        print("Failed to post to url %s" % url)


def process_json(json):
    sensor_id = json["id"]
    if sensor_id in SENSOR_WHITELIST_IDS:
        # Some sensors measure multiple things
        for key, value in json.items():
            if key in DATA_TO_UNIT_AND_ID:
                suffix = DATA_TO_UNIT_AND_ID[key]["suffix"]
                measurement_unit = DATA_TO_UNIT_AND_ID[key]["unit"]
                state = value
                entity = "sensor.rtl433_%s_%s" % (suffix, sensor_id)
                update_sensor_ha(entity, state, measurement_unit)


def start_rtl433():
    process = subprocess.Popen(["rtl_433", "-G", "-F", "json"],
                               stdout=subprocess.PIPE)
    while True:
        output = process.stdout.readline()
        json_object = None
        if output == '' and process.poll() is not None:
            break
        if output:
            try:
                json_object = json.loads(output)
                process_json(json_object)
            except ValueError as e:
                pass

    # Raise process exception if rc!=0
    rc = process.poll()
    return rc


def main():
    start_rtl433()

if __name__ == "__main__":
    main()



QE2 main focus, for the end of the half.

5015
11645
3559